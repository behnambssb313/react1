import React from 'react'

export default function Header({columns}) {
    
    const HeaderColumns=Object.keys(columns).map(key=>{
        return <th style={{width:columns[key].width}}>{columns[key].title}</th>
    })
    return (
        <thead>
            <tr>
                {HeaderColumns}
            </tr>
        </thead>
        
    )
}

import React,{useState} from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft,faAngleRight } from '@fortawesome/free-solid-svg-icons'

export default function Pageination({totalpage,currentpage,onchangepage,onchangelist,lilis}) {
    
    let [page,setpage]=useState(currentpage);
    let [pagee,setpagee]=useState(1);
    

    const Pageinator=[...new Array(totalpage).keys()].map(item=>{
        item++;
            return( 
                <li className={`page-item ${page === item ? 'page-current' : ''}`}>
                    <a onClick={(e)=>{setpage(item);onchangepage(e,item)}}>{item}</a>
                </li>
       )
        })

    const perPage=<select className="table-hover" style={{marginLeft:'40px',marginTop:'7px',backgroundColor:'#212529',borderColor:'#32383e',color:'white',height:'30px',width:'70px'}} onChange={(e)=>{setpage(currentpage);onchangelist(e,e.target.value);onchangepage(e,currentpage)}}>
    <option selected value="1">1</option>
    <option  value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
</select>;
    
    const prevPage=(e,page)=>{
        if(page!=currentpage){
            setpage(--page);
            onchangepage(e,page);
           
        }
    }
    const nextPage=(e,page)=>{
        if(page!=totalpage){
            setpage(++page);
            onchangepage(e,page);
        }
    }
 
    return (
        <div className="pageintaion">
            <ul>
            <li className="page-item"><a onClick={(e)=>{prevPage(e,page)}}><FontAwesomeIcon icon={faAngleLeft} /></a></li>
                {Pageinator}
                <li className="page-item"><a onClick={(e)=>{nextPage(e,page)}}><FontAwesomeIcon icon={faAngleRight} /></a></li>
                {perPage}
                <p style={{padding:'20px 0px 0px 10px',fontSize:'10px',margin:'0'}}>items per page</p>
                
            </ul>
        </div>
    )
}



import React,{useState} from 'react'
import Header  from "./Header";
import Tbody  from "./Tbody";
import Pageination from './Pageination'

export default function Table({data,columns,da}) {
    let [perPage,setperPage]=useState(1);
    let [source,setsource]=useState(data.slice(0,perPage))
    const currentPage=1;
    const totalPage=Math.round(data.length/perPage);


    const onchange=(e,item=1)=>{
        
        let offset=(item-1)*perPage;
        
        let spl=data.slice(offset,offset+parseInt(perPage));
        console.log(offset+parseInt(perPage))
        setsource(spl)

    }

   
    return (
        <div>
        <table className="table table-striped table-dark table-hover">
            <Header columns={columns}/>
            <Tbody items={source}/>
            <tr >
                <td colSpan="5" style={{padding:0}}>
                <Pageination
                  totalpage={totalPage}
                  currentpage={currentPage} 
                  onchangepage={(e,item)=>{onchange(e,item)}} 
                  onchangelist={(e,item)=>{ setperPage(perPage=item); }}/>
                </td>
            
            </tr>
            
        </table>
        </div>
    )
}

import React from 'react'

export default function Tbody({items}) {
    const body=items.map(item=>{
        return <tr>
            {Object.values(item).map(iii=>{return <td>{iii}</td>})}
        </tr>
    })

    
    return (
        <tbody>
            {body}
        </tbody>
    )
}
